package com.itgstoreconsulting.dev.biblionum.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Books")
public class Books implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "resume")
	private String resume;
	
	@Column(name = "authors")
	private String authors;
	
	@Column(name = "company")
	private String company;
	
	@Column(name = "dateEd")
	private LocalDate dateEd;
	
	@Column(name = "filename")
	private String filename;
	
	@Column(name = "version")
	private String version;

	public Books(){
		
	}
	
	public Books(String title, String resume, String authors, String company, LocalDate dateEd, String filename, String version){
		this.title = title;
		this.resume = resume;
		this.authors = authors;
		this.company = company;
		this.dateEd = dateEd;
		this.filename = filename;
		this.version = version;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public LocalDate getDateEd() {
		return dateEd;
	}

	public void setDateEd(LocalDate dateEd) {
		this.dateEd = dateEd;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Books [title=" + title + ", filename=" + filename + ", version=" + version + "]";
	}
}

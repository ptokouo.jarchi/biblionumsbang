package com.itgstoreconsulting.dev.biblionum.dao;

import java.io.Serializable;
import java.util.List;

import com.itgstoreconsulting.dev.biblionum.entities.Books;

public class BookPage implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<Books> books;
	
	private int pageSize;
	
	private long totalBooks;
	
	private int totalPages;
	
	private int page;

	public List<Books> getBooks() {
		return books;
	}

	public void setBooks(List<Books> books) {
		this.books = books;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalBooks() {
		return totalBooks;
	}

	public void setTotalBooks(long totalBooks) {
		this.totalBooks = totalBooks;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}

package com.itgstoreconsulting.dev.biblionum.solrdoc;

import javax.persistence.Id;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(solrCoreName = "bibNumeric")
public class SolrBooks{

	@Id
	@Field
	private String id;
	
	@Field
	private String title;
	
	@Field
	private String resume;
	
	@Field
	private String author;
	
	@Field
	private String company;
	
	@Field
	private String date_ed;
	
	@Field
	private String filename;
	
	@Field
	private String version;

	public SolrBooks(){
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDate_ed() {
		return date_ed;
	}

	public void setDateEd(String date_ed) {
		this.date_ed = date_ed;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Books [title=" + title + ", filename=" + filename + ", version=" + version + "]";
	}
}

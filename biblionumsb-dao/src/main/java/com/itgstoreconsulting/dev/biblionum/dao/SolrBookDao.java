package com.itgstoreconsulting.dev.biblionum.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

import com.itgstoreconsulting.dev.biblionum.solrdoc.SolrBooks;

public interface SolrBookDao extends SolrCrudRepository<SolrBooks, String> {

	@Query("title:*?0* OR resume:*?0*")
    public Page<SolrBooks> getBooks(String crit, Pageable pageable);	
}

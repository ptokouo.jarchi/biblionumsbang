package com.itgstoreconsulting.dev.biblionum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.itgstoreconsulting.dev.biblionum.dao.SolrBookDao;
import com.itgstoreconsulting.dev.biblionum.solrdoc.SolrBooks;

@SpringBootApplication
public class BiblionumsbDaoApplication {

	public static void main(String[] args) {
		ApplicationContext context =  SpringApplication.run(BiblionumsbDaoApplication.class, args);
		SolrBookDao solrBookDao = context.getBean(SolrBookDao.class);
		Page<SolrBooks> books = solrBookDao.getBooks("Java", PageRequest.of(0, 10, new Sort(Direction.DESC, "date_ed")));
		books.stream().forEach(b -> System.out.println(b));
	}
}

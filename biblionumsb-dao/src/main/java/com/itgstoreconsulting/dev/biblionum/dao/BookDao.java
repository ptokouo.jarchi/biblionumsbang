package com.itgstoreconsulting.dev.biblionum.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.itgstoreconsulting.dev.biblionum.entities.Books;

public interface BookDao extends JpaRepository<Books, String>{
	
	@Query("select b from Books b where title like :crit or resume like :crit or authors like :crit or company like :crit")
	public Page<Books> getBooks(@Param("crit") String crit, Pageable pageable);
	
	public Books findById(long id);
}
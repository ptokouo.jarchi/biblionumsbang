package com.itgstoreconsulting.dev.biblionum.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiblionumsbWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiblionumsbWebApplication.class, args);
	}
}

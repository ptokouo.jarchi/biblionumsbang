var app = angular.module("BiblioApp", []);

app.controller("BiblioController", function($scope, $http){
	$scope.edSearch = "Java";
	$scope.currentPage = 0;
	$scope.size = 4;
	$scope.bookPages = [];
	$scope.totalElements = 0;
	$scope.typeSource = "postgre";
	
	$scope.findBook = function(){
		$http({
            method : 'GET',
            url : 'http://localhost:8081/BIBLIONUM/books/get?criteria=' + $scope.edSearch + '&page=' + $scope.currentPage + '&size=' + $scope.size + "&source=" + $scope.typeSource
        }).then(function successCallback(response) {
            $scope.bookPages = response.data;
            $scope.pages = new Array(response.data.totalPages);
            $scope.totalElements = response.data.totalBooks;
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	}
	
	$scope.downloadBook = function(b){
		$http({
            method : 'GET',
            url : 'http://localhost:8081/BIBLIONUM/books/download?id=' + b,
            responseType:'arraybuffer'
        }).then(function successCallback(response) {
        	var file = new Blob([response.data], {type: 'application/pdf'});
            var fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	}
	
	$scope.goToPage = function(p){
		$scope.currentPage = p;
		$scope.findBook();
	}
	
	$scope.saveBook = function(){
		$http({
			method : 'PUT',
            url : 'http://localhost:8081/BIBLIONUM/books/add',
            data: 'title=' + $scope.edTitle + '&resume=' + $scope.edResume + "&authors=" + $scope.edAuthors 
            	+ "&company=" + $scope.edCompany + "&filename=" + $scope.edFilename + "&version=" + $scope.edVersion,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.findBook();
            $scope.resetForm();
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	}
	
	$scope.resetForm = function(){
        $scope.edTitle = "";
        $scope.edResume = "";
        $scope.edAuthors = "";
        $scope.edCompany = "";
        $scope.edFilename = "";
        $scope.edVersion = "";
	}
});
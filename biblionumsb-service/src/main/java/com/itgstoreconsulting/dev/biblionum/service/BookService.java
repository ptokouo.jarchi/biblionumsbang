package com.itgstoreconsulting.dev.biblionum.service;

import com.itgstoreconsulting.dev.biblionum.dao.BookPage;
import com.itgstoreconsulting.dev.biblionum.entities.Books;

public interface BookService {

	public Books addBook(Books book);
	
	public BookPage findBooks(String criteria, int page, int size, String source);
	
	public Books getBookById(long id);
}

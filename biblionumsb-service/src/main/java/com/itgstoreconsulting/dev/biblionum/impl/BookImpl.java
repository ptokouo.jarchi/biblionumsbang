package com.itgstoreconsulting.dev.biblionum.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.itgstoreconsulting.dev.biblionum.dao.BookDao;
import com.itgstoreconsulting.dev.biblionum.dao.BookPage;
import com.itgstoreconsulting.dev.biblionum.dao.SolrBookDao;
import com.itgstoreconsulting.dev.biblionum.entities.Books;
import com.itgstoreconsulting.dev.biblionum.service.BookService;
import com.itgstoreconsulting.dev.biblionum.solrdoc.SolrBooks;

@Service
public class BookImpl implements BookService{

	@Autowired
	private BookDao bookDao;
	
	@Autowired
	private SolrBookDao solrBookDao;
	
	@Override
	public Books addBook(Books book) {
		return bookDao.save(book);		
	}

	@Override
	public BookPage findBooks(String criteria, int page, int size, String source) {
		BookPage bookPage = new BookPage();
		if("postgre".equals(source)){
			Page<Books> pages = bookDao.getBooks("%" + criteria + "%", PageRequest.of(page, size, new Sort(Direction.DESC, "dateEd")));
			bookPage.setBooks(pages.getContent());
			bookPage.setPage(pages.getNumber());
			bookPage.setPageSize(pages.getTotalPages());
			bookPage.setTotalBooks(pages.getTotalElements());
			bookPage.setTotalPages(pages.getTotalPages());
		}
		else{
			Page<SolrBooks> pages = solrBookDao.getBooks(criteria, PageRequest.of(page, size, new Sort(Direction.DESC, "date_ed")));
			List<SolrBooks> solrBooks = pages.getContent();
			List<Books> books = new ArrayList<>();
			solrBooks.stream().forEach(sb -> 
				books.add(new Books(sb.getTitle(), sb.getResume(), sb.getAuthor(), sb.getCompany(), LocalDate.now(), sb.getFilename(), sb.getVersion()))
			);			
			bookPage.setBooks(books);
			bookPage.setPage(pages.getNumber());
			bookPage.setPageSize(pages.getTotalPages());
			bookPage.setTotalBooks(pages.getTotalElements());
			bookPage.setTotalPages(pages.getTotalPages());
		}
		return bookPage;
	}

	@Override
	public Books getBookById(long id) {
		return bookDao.findById(id);
	}
}

package com.itgstoreconsulting.dev.biblionum.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiblionumsbServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiblionumsbServiceApplication.class, args);
	}
}

package com.itgstoreconsulting.dev.biblionum.publish;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itgstoreconsulting.dev.biblionum.dao.BookPage;
import com.itgstoreconsulting.dev.biblionum.entities.Books;
import com.itgstoreconsulting.dev.biblionum.service.BookService;

@RestController
@RequestMapping("/BIBLIONUM")
public class BiblioRestService {

	@Autowired
	private BookService bookService;
	
	@Value("${biblionum.jcr.directory}")
	private String jcrDirectory;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "books/add", method=RequestMethod.PUT)
	public void addBook(@RequestParam("title") String title, 
			@RequestParam("resume") String resume, 
			@RequestParam("authors") String authors, 
			@RequestParam("company") String company, 
			@RequestParam("filename") String filename, 
			@RequestParam("version") String version) {
		
		Books book = new Books(title, resume, authors, company, LocalDate.now(), filename, version);
		
		bookService.addBook(book);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "books/get", method = RequestMethod.GET)
	public BookPage findBooks(@RequestParam("criteria") String criteria, 
			@RequestParam("page") int page, 
			@RequestParam("size") int size,
			@RequestParam("source") String source) {
		return bookService.findBooks(criteria, page, size, source);
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "books/download", method = RequestMethod.GET)
	public ResponseEntity<Resource> download(@RequestParam("id") String id) throws IOException {
		Books book = bookService.getBookById(Long.valueOf(id));		
		File file = new File(jcrDirectory + book.getFilename());

	    Path path = Paths.get(file.getAbsolutePath());
	    ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

	    return ResponseEntity.ok()
	            .contentLength(file.length())
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}
}
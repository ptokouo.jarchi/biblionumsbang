package com.itgstoreconsulting.dev.biblionum.publish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.itgstoreconsulting.dev.biblionum.dao.BookPage;

@SpringBootApplication
@ComponentScan({"com.itgstoreconsulting.dev.biblionum"})
public class BiblionumsbPublishApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(BiblionumsbPublishApplication.class, args);
		BiblioRestService biblioServ = context.getBean(BiblioRestService.class);
		BookPage bp = biblioServ.findBooks("Java", 0, 10, "solr");
		bp.getBooks().stream().forEach(b -> System.out.println(b));
	}
}
